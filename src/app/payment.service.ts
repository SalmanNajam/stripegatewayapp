import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PaymentService {
  constructor(private httpClient: HttpClient) {}
  ChargePayment(token: string): Observable<any> {
    return this.httpClient.get<any>(
      'https://stripepaymentapi.educationalinstitutes.net/api/payment/' + token
    );
  }
}
