import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { StripeService, StripeCardComponent, ElementOptions, ElementsOptions } from 'ngx-stripe';
import { PaymentService } from '../payment.service';

@Component({
  selector: 'app-stripe-test',
  templateUrl: './stripe-test-component.component.html',
  styleUrls: ['./stripe-test-component.component.css']
})

export class StripeTestComponent implements OnInit {
  @ViewChild(StripeCardComponent) card: StripeCardComponent;
  returnedObject: any;
  alertClass: string;
  alertText: string;
  isHidden = false;
  cardOptions: ElementOptions = {
    style: {
      base: {
        iconColor: '#666EE8',
        color: '#31325F',
        lineHeight: '40px',
        fontWeight: 300,
        fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
        fontSize: '18px',
        '::placeholder': {
          color: 'grey'
        }
      }
    }
  };

  elementsOptions: ElementsOptions = {
    locale: 'en'
  };

  stripeTest: FormGroup;

  constructor(
    private fb: FormBuilder,
    private paymentService: PaymentService,
    private stripeService: StripeService) { }

  ngOnInit() {
    this.stripeTest = this.fb.group({
      name: ['', [Validators.required]]
    });
  }

  buy() {
    const name = this.stripeTest.get('name').value;
    this.stripeService
      .createToken(this.card.getCard(), { name })
      .subscribe(result => {
        if (result.token) {
          this.paymentService.ChargePayment(result.token.id).subscribe(
            (response) => {
              this.isHidden = true;
              this.alertClass = 'alert alert-success';
              this.returnedObject = response.status;
              this.alertText = 'Payment Successful!';
              console.log('Status: ', this.returnedObject);
            },
            (error) => {
              this.isHidden = true;
              this.alertClass = 'alert alert-danger';
              this.alertText = 'An error occurred while processing transaction!';
              console.log('An error occurred while processing transaction!');
            }
          );
        } else if (result.error) {
          this.isHidden = true;
          this.alertClass = 'alert alert-danger';
          this.alertText = result.error.message;
          console.log(result.error.message);
        }
      });
  }
}
